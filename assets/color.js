window.myNamespace = Object.assign({}, window.myNamespace, {
    geojsonExample: {
        style: function(feature) {
            if(feature.properties.enter === 1){
                return {color:'red'}
            }
            return {color:'grey'}
        }
    }
});
