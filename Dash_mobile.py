import sys
import dash_html_components as html
import dash_leaflet as dl
from dash import Dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import psycopg2
import json
import requests
import warnings
warnings.filterwarnings("ignore")

conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                        database="postgres")


def sher_dash(user_id, port):
    if not user_id:
        return 'No user_id provided'

    if not port:
        return 'No port found for provided user'

    cur = conn.cursor()
    cur.execute(
        'select A.id_ing,B."Lat",B."Long"  from "Ingeneers" as A inner join "Coordinates" as B on A."id_ing"=B."ID" where B."Type"=2 and A.user_id={}'.format(
            user_id))
    coords = cur.fetchall()[0]
    cur.close()

    Sher_coordinates = (coords[1], coords[2])
    snow_coords = []
    snow_track = ''
    snow_height = 0
    snow_clicks = 0

    interval1 = dcc.Interval(
        id='interval-component',
        interval=60 * 1000,  # in milliseconds
        n_intervals=0
    )

    timer = html.Div(
        html.Div([
            interval1
        ])
    )

    def get_info(sam,pos,tpe):
        header = [html.H5(tpe+" " + sam),html.B( '{:.5f} / {:.5f}'.format(pos[0],pos[1]))]
        return header
    def get_positions():
        conn = psycopg2.connect(host= "134.209.236.228",port= "5432",user= "knova",password= "hack21052021",database= "postgres")

        cur = conn.cursor()
        cur.execute('select  A."id_c", A."ID",A."Lat",A."Long",C."user_id",C."Name" from "Coordinates" as A inner join "Ingeneers" as B on A."ID"=B."id_ing" inner join "Users" as C on B."user_id"=C."user_id" where A."Type"=2')
    #cur.execute("ROLLBACK")
        res_p=cur.fetchall()
        iconUrl = "https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/man.png"
        samolets=[dl.Marker(id=str(x[0]),position=[x[2],x[3]],icon=dict(iconUrl=iconUrl, iconAnchor=[10, 10]),
                 children=dl.Tooltip(get_info(str(x[0]) + ' ' +x[5],[x[2],x[3]],'Инженер'))) for x in res_p]
        cur.execute('select  A."id_c", A."ID",A."Lat",A."Long",B."Car_name",B."Car_type",C."Name_type",C."Logo" from "Coordinates" as A inner join "Car" as B on A."ID"=B."id_car" inner join "Car_type" as C on B."Car_type"=C."id_type" where A."Type"=1')
    #cur.execute("ROLLBACK")
        res_p=cur.fetchall()
        samolets1=[dl.Marker(id=str(x[0]),position=[x[2],x[3]],icon=dict(iconUrl=x[7], iconAnchor=[10, 10]),
                 children=dl.Tooltip(get_info(str(x[1]) + ' ' +x[4],[x[2],x[3]],x[6]))) for x in res_p]
        cur.execute('select  A."id_c", A."ID",A."Lat",A."Long",B."Plane_name",B."Logo" from "Coordinates" as A inner join "Plane" as B on A."ID"=B."plane_id"  where A."Type"=0')
        res_p=cur.fetchall()
        samolets2=[dl.Marker(id=str(x[0]),position=[x[2],x[3]],icon=dict(iconUrl=x[5], iconAnchor=[10, 10]),
                 children=dl.Tooltip(get_info(str(x[1]) + ' ' +x[4],[x[2],x[3]],'Самолет'))) for x in res_p]
        cur.close()
        conn.close()
        return samolets+samolets1+samolets2

    planes=get_positions()

    inp = dcc.Input(
        placeholder='Enter a value...',
        type='number',
        value=0,
        id='input_v'
    )

    btn = html.Button('Записать', id='button')

    task = html.Div(
        html.Div([
            html.H3(children='Track is not selected', id='H1'),
            inp, btn
        ])
    )

    url = 'https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/GeoJSON_sher.json'
    resp = requests.get(url)
    data = json.loads(resp.text)

    url = 'https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/karta_sher.json'
    resp = requests.get(url)
    data2 = json.loads(resp.text)

    for k in range(len(data['features'])):
        data['features'][k]['properties']['enter'] = 0
    gss = []
    gss_dic = dict()
    for el in range(len(data['features'])):
        data1 = {'type': 'FeatureCollection', 'features': [data['features'][el]]}
        data1n = data['features'][el]['properties']['name']
        gs1 = dl.GeoJSON(data=data1, options=dict(color='#32a852'), children=dl.Tooltip(data1n), id=data1n)
        gss.append(gs1)
        gss_dic[data1n] = el

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = Dash(prevent_initial_callbacks=True, external_stylesheets=external_stylesheets)
    app.layout = html.Div([timer,
                           dl.Map([dl.TileLayer(), html.Div(gss),
                                   html.Div(planes, id='carplanes'),
                                   dl.LayerGroup(id="layer"), dl.LayerGroup(id="layer_pol")],
                                  id="map", center=Sher_coordinates, zoom=15,
                                  style={'width': '100%', 'height': '50vh', 'margin': "auto", "display": "block"}),
                           task
                           ])

    @app.callback(Output('map', 'center'),
                  Input('interval-component', 'n_intervals'))
    def update_metrics(n):
        cur = conn.cursor()
        cur.execute(
            'select A.id_ing,B."Lat",B."Long"  from "Ingeneers" as A inner join "Coordinates" as B on A."id_ing"=B."ID" where B."Type"=2 and A.user_id={}'.format(
                3))
        coords = cur.fetchall()[0]
        cur.close()
        nonlocal Sher_coordinates
        Sher_coordinates = (coords[1], coords[2])
        return Sher_coordinates

    @app.callback(Output("H1", "children"),
                  [Input("map", "click_lat_lng"), Input("input_v", "value")])
    def map_click(click_lat_lng, vals):
        url = 'http://134.209.236.228:8100/api/getPolygon?x0={}&y0={}'.format(click_lat_lng[0], click_lat_lng[1])
        nonlocal snow_track
        nonlocal snow_coords
        nonlocal snow_height
        snow_track = requests.get(url).text
        snow_coords = click_lat_lng
        snow_height = vals
        if len(snow_track) > 0:
            return 'Snow is in {:.5f},{:.5f} in track {} with height {}'.format(snow_coords[0], snow_coords[1],
                                                                                snow_track, snow_height)
        else:
            return 'Track is not selected'

    @app.callback(Output("layer", "children"),
                  [Input('button', 'n_clicks')])
    def btn_click(nclicks):
        if (len(snow_track) > 0) & (snow_height > 0):
            print(snow_height, snow_track)
            cur = conn.cursor()
            cur.execute(
                'UPDATE "Track" set "Ing"=True, "Val"={} where "Track_name"=\'{}\''.format(snow_height, snow_track))
            conn.commit()
            cur.close()
        return []

    app.run_server(host='0.0.0.0', port=port, debug=False)


if __name__ == '__main__':
    port = sys.argv[1]
    user_id = sys.argv[2]
    sher_dash(user_id, port)