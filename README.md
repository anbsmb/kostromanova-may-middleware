Подробнее в
<a href="https://gitlab.com/anbsmb/kostromanova-may-joint">основном репозитории</a> 

Python files for Sheremetyevo projects.
- Sheremetyevo dashboard
- Yandex meteo widget

Nedeed libraries:

Brotli==1.0.9
click==7.1.2
dash==1.20.0
dash-core-components==1.16.0
dash-html-components==1.1.3
dash-leaflet==0.1.13
dash-renderer==1.9.1
dash-table==4.11.3
Flask==1.1.2
Flask-Compress==1.9.0
future==0.18.2
geobuf==1.1.1
itsdangerous==1.1.0
Jinja2==2.11.3
MarkupSafe==1.1.1
numpy==1.20.2
plotly==4.14.3
protobuf==3.16.0
retrying==1.3.3
six==1.16.0
Werkzeug==1.0.1

