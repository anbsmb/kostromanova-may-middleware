import sys
import dash_html_components as html
import dash_leaflet as dl
from dash import Dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import datetime
import json
import requests
import psycopg2
import numpy as np


def sher_dash(port):
    if not port:
        return 'No port found for provided user'

    c1 = 55.97250710300642
    c2 = 37.413000366210945
    Sher_coordinates = (c1, c2)

    interval1 = dcc.Interval(
        id='interval-component',
        interval=1 * 1000,  # in milliseconds
        n_intervals=0
    )

    timer = html.Div(
        html.Div([
            # html.H1(children='The time is: '+str(datetime.datetime.now()) ,id='H1'),
            interval1
        ])
    )

    sldr = html.Div(dcc.Slider(id='my-slider', min=0, max=4, step=0.5, value=0,
                               marks={0: 0, 0.5: 0.5, 1: 1, 1.5: 1.5, 2: 2, 2.5: 2.5, 3: 3, 3.5: 3.5, 4: 4}))

    def get_info(sam, pos, tpe):
        header = [html.H5(tpe + " " + sam), html.B('{:.5f} / {:.5f}'.format(pos[0], pos[1]))]
        return header

    def get_positions():
        conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                                database="postgres")

        cur = conn.cursor()
        cur.execute(
            'select  A."id_c", A."ID",A."Lat",A."Long",C."user_id",C."Name" from "Coordinates" as A inner join "Ingeneers" as B on A."ID"=B."id_ing" inner join "Users" as C on B."user_id"=C."user_id" where A."Type"=2')
        # cur.execute("ROLLBACK")
        res_p = cur.fetchall()
        iconUrl = "https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/man.png"
        samolets = [dl.Marker(id=str(x[0]), position=[x[2], x[3]], icon=dict(iconUrl=iconUrl, iconAnchor=[10, 10]),
                              children=dl.Tooltip(get_info(str(x[0]) + ' ' + x[5], [x[2], x[3]], 'Инженер'))) for x in
                    res_p]
        cur.execute(
            'select  A."id_c", A."ID",A."Lat",A."Long",B."Car_name",B."Car_type",C."Name_type",C."Logo" from "Coordinates" as A inner join "Car" as B on A."ID"=B."id_car" inner join "Car_type" as C on B."Car_type"=C."id_type" where A."Type"=1')
        # cur.execute("ROLLBACK")
        res_p = cur.fetchall()
        samolets1 = [dl.Marker(id=str(x[0]), position=[x[2], x[3]], icon=dict(iconUrl=x[7], iconAnchor=[10, 10]),
                               children=dl.Tooltip(get_info(str(x[1]) + ' ' + x[4], [x[2], x[3]], x[6]))) for x in
                     res_p]
        cur.execute(
            'select  A."id_c", A."ID",A."Lat",A."Long",B."Plane_name",B."Logo" from "Coordinates" as A inner join "Plane" as B on A."ID"=B."plane_id"  where A."Type"=0')
        res_p = cur.fetchall()
        samolets2 = [dl.Marker(id=str(x[0]), position=[x[2], x[3]], icon=dict(iconUrl=x[5], iconAnchor=[10, 10]),
                               children=dl.Tooltip(get_info(str(x[1]) + ' ' + x[4], [x[2], x[3]], 'Самолет'))) for x in
                     res_p]
        cur.close()
        conn.close()
        return samolets + samolets1 + samolets2

    planes = get_positions()

    url = 'https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/GeoJSON_sher.json'
    resp = requests.get(url)
    data = json.loads(resp.text)

    url = 'https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/karta_sher.json'
    resp = requests.get(url)
    data2 = json.loads(resp.text)

    def get_reserve(tek_dat):
        conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                                database="postgres")
        cur = conn.cursor()
        cur.execute(
            'select A.*,B."Plane_name" from "Plane_reserve" as A inner join "Plane" as B on A."Plane"=B."plane_id"  ')
        res_p = np.array(cur.fetchall()).T
        res_p = res_p.T[(res_p[2] <= tek_dat) & (res_p[3] >= tek_dat)]
        itog_pl = dict()
        for el in res_p:
            sss = el[-1] + ' from ' + el[2].strftime("%H:%M") + ' to ' + el[3].strftime("%H:%M")
            if el[4] is None:
                itog_pl[el[5]] = sss
            else:
                cur.execute('select "Track_name" from "Track" where "Line"=\'{}\''.format(el[4]))
                for xx in cur.fetchall():
                    itog_pl[xx[0]] = sss
        cur.close()
        conn.close()
        return [dl.CircleMarker(center=np.mean(data2[el], axis=0), radius=15, color='#0000FF', fillColor='#0000FF',
                                children=dl.Tooltip(itog_pl[el])) for el in itog_pl]

    CMs = get_reserve(datetime.datetime.today())

    def get_problems():
        conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                                database="postgres")
        cur = conn.cursor()
        cur.execute(
            'select A."Track_name", A."Snow_height",A."Ing" , A."Val", B."id_rule", B."Name" from "Track" as A inner join "Rules" as B on (((A."Snow_height">=B."Snow_height_min") and (A."Snow_height"<=B."Snow_height_max")) or ((A."Ing" and B."Ing")  and ((A."Val">=B."Val_height_min") and (A."Val"<=B."Val_height_max"))))')
        # cur.execute("ROLLBACK")
        res_p = cur.fetchall()
        cur.close()
        conn.close()
        rules1 = set([(el[4], el[5]) for el in res_p])
        return [dl.CircleMarker(center=np.mean(data2[el[0]], axis=0), radius=12, color='#FF0000', children=dl.Tooltip(
            "Snow height is {}, Engineer recomendation is {}, Snow shaft is {}. Rule {} connected".format(el[1], el[2],
                                                                                                          el[3],
                                                                                                          el[5]))) for
                el in res_p], rules1

    PMs, RLs = get_problems()
    for k in range(len(data['features'])):
        data['features'][k]['properties']['enter'] = 0
    gss = []
    gss_dic = dict()
    for el in range(len(data['features'])):
        data1 = {'type': 'FeatureCollection', 'features': [data['features'][el]]}
        data1n = data['features'][el]['properties']['name']
        gs1 = dl.GeoJSON(data=data1, options=dict(color='#32a852'), children=dl.Tooltip(data1n), id=data1n)
        gss.append(gs1)
        gss_dic[data1n] = el

    zadan = list(RLs)[0][0]
    dde_m = [{'label': el[1], 'value': el[0]} for el in RLs]
    dde = dcc.Dropdown(options=dde_m,
                       value=list(RLs)[0][0],
                       id='vibor_u'
                       )
    btn = html.Button('Отправить на очистку', id='button')

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = Dash(prevent_initial_callbacks=True, external_stylesheets=external_stylesheets)
    app.layout = html.Div([timer, sldr,
                           dl.Map([dl.TileLayer(), html.Div(gss),
                                   html.Div(planes, id='carplanes'), html.Div(CMs, id='reserved'),
                                   html.Div(PMs, id='problems'),
                                   dl.LayerGroup(id="layer"), dl.LayerGroup(id="layer_pol")],
                                  id="map", center=Sher_coordinates, zoom=15,
                                  style={'width': '100%', 'height': '80vh', 'margin': "auto", "display": "block"}),
                           html.Div([dde, btn])
                           ])

    def get_polygon(x0, y0):
        for k in data2:
            ss = np.array(data2[k]).T
            x1 = ss[0][1:]
            x2 = ss[0][:-1]
            y1 = ss[1][1:]
            y2 = ss[1][:-1]
            xt = (y0 - y1) / (y2 - y1) * (x2 - x1) + x1
            if np.sum(((xt <= x0) & ((x2 - xt) * (xt - x1) >= 0) & ((y2 - y0) * (y0 - y1) >= 0)) | (
                    (y1 == y0) & (y2 == y0) & (((x1 - x0) * (x0 - x2)) > 0))) % 2 == 1:
                return k
        return None

    @app.callback(Output('reserved', "children"), [Input('my-slider', "value")])
    def reserved_pr(vls):
        tek_dat = datetime.datetime.today() + datetime.timedelta(minutes=vls * 60)
        tek_dat = datetime.datetime(tek_dat.year, tek_dat.month, tek_dat.day, tek_dat.time().hour,
                                    tek_dat.time().minute)
        return get_reserve(tek_dat)

    @app.callback(Output('carplanes', 'children'),
                  Input('interval-component', 'n_intervals'))
    def update_metrics(n):
        return get_positions()

    @app.callback(Output("layer", "children"),
                  Input('button', 'n_clicks'))
    def update_button(n):
        conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                                database="postgres")
        cur = conn.cursor()
        cur.execute(
            'insert into "Task" ("Accepted", "StatusId", "PriorityId", "task_name") values (False, 1, 2, \'{}\') returning "id_task"'.format("Уборка снега"))
        new_task = cur.fetchone()[0]
        cur.execute('select "Car_type" from "Rule_cars" where "id_rule"={}'.format(zadan))
        res_car = cur.fetchall()
        for el in res_car:
            cur.execute('insert into "Task_cars" ("Task","Car_type_id") values ({}, {})'.format(new_task, el[0]))
        for k in range(len(data['features'])):
            fffgh = data['features'][k]['properties']
            if fffgh['enter'] == 1:
                cur.execute(
                    'insert into "Task_tracks" ("Task","Track") values ({}, \'{}\')'.format(new_task, fffgh['name']))
        conn.commit()
        cur.close()
        conn.close()
        return []

    @app.callback(Output("layer_pol", "children"),
                  Input('vibor_u', 'value'))
    def update_vibor(n):
        nonlocal zadan
        zadan = n
        return []

    for el in range(len(data['features'])):
        data1n = data['features'][el]['properties']['name']

        @app.callback(Output(data1n, "options"), [Input(data1n, "click_feature")])
        def capital_click(feature):
            if feature is not None:
                if gss[gss_dic[feature['properties']['name']]].data['features'][0]['properties']['enter'] == 0:
                    clr = '#ff0000'
                    gss[gss_dic[feature['properties']['name']]].data['features'][0]['properties']['enter'] = 1
                else:
                    clr = '#32a852'
                    gss[gss_dic[feature['properties']['name']]].data['features'][0]['properties']['enter'] = 0
                return dict(color=clr)

        @app.callback(Output(data1n, "click_feature"), [Input(data1n, "options")])
        def back_click(feature):
            return None

    app.run_server(host='0.0.0.0', port=port, debug=False)


if __name__ == '__main__':
    port = sys.argv[1]
    sher_dash(port)
