from flask import Flask, request
from flask_cors import CORS, cross_origin
import psycopg2
import os

conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                        database="postgres")

flask_app = Flask(__name__)
CORS(flask_app, support_credentials=True)
maps_opened = dict()




@flask_app.route('/api/startMap')
@flask_app.route('/api/mobile/startMap')
@cross_origin(supports_credentials=True)
def start_dash_server():
    global maps_opened
    user_id = request.args.get('userId', type=int)
    if not user_id:
        return 'No user id provided'

    exists = maps_opened.get(user_id, None)
    if exists:
        return exists.get('url', None)

    cur = conn.cursor()
    try:
        cur.execute(f'SELECT "Port" FROM "Users" WHERE user_id = {user_id}')
        port = cur.fetchone()[0]
        cur.close()
        maps_opened[user_id] = {
            "port": port,
            "url": f'http://134.209.236.228:{port}'
        }

        os.system(f'sudo python3 {os.getcwd()}/{"Dash_mobile" if "mobile" in request.path else "Dash"}.py {port} {user_id} &')
        return maps_opened[user_id]["url"]
    except Exception as e:
        if user_id in maps_opened:
            del maps_opened[user_id]
        return e


@flask_app.route('/api/getMap')
@flask_app.route('/api/mobile/getMap')
@cross_origin(supports_credentials=True)
def get_dash_server():
    global maps_opened
    user_id = request.args.get('userId', type=int)
    if not user_id:
        return 'No user id provided'

    exists = maps_opened.get(user_id, None)
    if exists:
        return exists.get('url', None)

    return 'No map exists for this user'


@flask_app.route('/api/closeMap')
@flask_app.route('/api/mobile/closeMap')
@cross_origin(supports_credentials=True)
def shutdown_dash_server():
    global maps_opened
    user_id = request.args.get('userId', type=int)
    print(user_id)
    print(maps_opened)
    if user_id and maps_opened.get(user_id, None):
        port, url = maps_opened[user_id].values()
        del maps_opened[user_id]
        os.system(f'sudo lsof -i -P -n | grep {port} | cut -d " " -f 4')
        os.system(f"""if [ "$(sudo lsof -i -P -n | grep {port})" != "" ]; then kill -9 `sudo lsof -i -P -n | grep {port} | cut -d " " -f 4`; fi""")
        return 'success'
    return ''


if __name__ == '__main__':
    flask_app.run(host='0.0.0.0', port='8150', debug=True)
